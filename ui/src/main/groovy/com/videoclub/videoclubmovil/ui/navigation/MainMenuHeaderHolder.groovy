package com.videoclub.videoclubmovil.ui.navigation

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Learning-03 on 04/06/2015.
 *
 * Represents the {@link android.support.v7.widget.RecyclerView.ViewHolder} item.
 */
class MainMenuHeaderHolder extends RecyclerView.ViewHolder {

    /// region Constructors

    /**
     * Initializes the new holder instance with specified view.
     * @param itemView Current view.
     */
    MainMenuHeaderHolder(View itemView) {
        super(itemView)
    }

    /// endregion Constructors
}
