package com.videoclub.videoclubmovil.ui.navigation

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.transform.TupleConstructor

/**
 * Created by Learning-03 on 04/06/2015.
 *
 * Represents a simple main menu item.
 */
@EqualsAndHashCode
@TupleConstructor
@ToString(includeFields = true, includeNames = true, includePackage = true)
class MainMenuItem {

    /// region Properties

    /**
     * The android icon resource id.
     */
    int iconResId

    /**
     * The string object that represents the item text.
     */
    String text

    /// endregion Properties

}
