package com.videoclub.videoclubmovil.ui

import android.content.Intent
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.gc.materialdesign.views.ButtonFlat
import com.videoclub.videoclubmovil.ui.login.LoginActivity
import groovy.transform.CompileStatic

/**
 * Created by Learning-03 on 27/05/2015.
 *
 * Represents the first fragment to be show in the main activity.
 */
@CompileStatic
final class MainActivityFirsFragment extends Fragment {
    /// region Variables

    /**
     * Current button.
     */
    private ButtonFlat buttonFlat

    /// endregion Variables

    /// region Constructors

    /**
     * Initializes the new object.
     */
    MainActivityFirsFragment() {

    }

    /// endregion Constructors

    /// region Public methods

    /// region Override methods

    /**
     * Let's create custom view for this fragment.
     * @param inflater Current layout inflater.
     * @param container Current layout container.
     * @param savedInstanceState If isn't null, this fragment is being re-constructed from previous state.
     * @return New created view or null.
     */
    @Override
    View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        def view = inflater.inflate(R.layout.main_activity_first_fragment, container, false)

        def position = this.getArguments().getInt('position')

        def textView = view.findViewById(R.id.main_activity_first_fragment_textView) as TextView

        def imageView = view.findViewById(R.id.main_activity_first_fragment_imageview) as ImageView

        if (position == 3) {
            this.buttonFlat = view.findViewById(R.id.buttonComenzar) as ButtonFlat
            this.buttonFlat.visibility = ButtonFlat.VISIBLE
        }

        textView.setText(getTextByPosition(position))

        imageView.setImageResource(getImageResourceByPosition(position))

        view
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    void onResume() {
        super.onResume()

        if (this.buttonFlat != null) {
            this.buttonFlat.onClickListener = {
                def intent = new Intent(activity, LoginActivity.class)
                activity.startActivity(intent)
            }
        }
    }

    /// endregion Override methods]

    /// endregion Public methods

    /// region Private methods

    /**
     * Attempts to get the corresponding position text from resources.
     * @return Matched text.
     */
    private final String getTextByPosition(position) {

        switch (position) {
            case 0: return this.resources.getString(R.string.main_activity_first_fragment_textview_text1)

            case 1: return this.resources.getString(R.string.main_activity_first_fragment_textview_text2)

            case 2: return this.resources.getString(R.string.main_activity_first_fragment_textview_text3)

            default: return this.resources.getString(R.string.main_activity_first_fragment_textview_text4)
        }

    }

    /**
     * Attempts to get the corresponding resource number from position.
     * @param position Current position.
     * @return Resource image number.
     */
    private final int getImageResourceByPosition(position) {
        switch (position) {
            case 0: return R.drawable.camera

            case 1: return R.drawable.camera2

            case 2: return R.drawable.rocker

            default: return R.drawable.nuclear_mushroom
        }
    }

    /// endregion Private static methods
}
