package com.videoclub.videoclubmovil.ui.movies

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.videoclub.videoclubmovil.ui.R

/**
 * Created by Learning-03 on 05/06/2015.
 *
 * Represents the {@link android.support.v7.widget.RecyclerView.ViewHolder} item.
 */
class MovieCardViewHolder extends RecyclerView.ViewHolder {

    /// region Constructors

    /**
     * Initializes a new instance.
     * @param itemView Current movie card view.
     */
    MovieCardViewHolder(View itemView) {
        super(itemView)

        this.movieImage = itemView.findViewById(R.id.imageViewMovie) as ImageView

        this.movieAvailability = itemView.findViewById(R.id.textViewAvailability) as TextView

        this.movieGenres = itemView.findViewById(R.id.textViewMovieGenres) as TextView

        this.movieTitlte = itemView.findViewById(R.id.textViewMovieTitle) as TextView
    }

    /// endregion Constructors

    /// region Properties

    /**
     * The movie image.
     */
    ImageView movieImage

    /**
     * The movie title.
     */
    TextView movieTitlte

    /**
     * {@link TextView} to show the movie genres.
     */
    TextView movieGenres

    /**
     * {@link TextView} to show the movie availability.
     */
    TextView movieAvailability

    /// endregion Properties
}
