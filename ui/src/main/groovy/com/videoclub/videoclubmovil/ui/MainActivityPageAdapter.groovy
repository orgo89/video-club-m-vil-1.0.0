package com.videoclub.videoclubmovil.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import groovy.transform.CompileStatic

/**
 * Created by Learning-03 on 27/05/2015.
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
 * sections of the app.
 */
@CompileStatic
final class MainActivityPageAdapter extends FragmentPagerAdapter {
    /// region Variables

    /// endregion Variables

    /// region Constructors

    /**
     * Initializes the new object.
     * @param fragmentManager Current fragment manager.
     * @param currentContext Current activity context.
     */
    MainActivityPageAdapter(FragmentManager fragmentManager) {
        super(fragmentManager)
    }

    /// endregion Constructors

    /// region Public methods

    /// region FragmentPagerAdapter members

    /**
     * Return the number of views available.
     */
    @Override
    int getCount() {
        return 4
    }

    /**
     * Return the Fragment associated with a specified position.
     */
    @Override
    Fragment getItem(int position) {
        def fragment = new MainActivityFirsFragment()

        def bundle = new Bundle()

        bundle.putInt('position', position)

        fragment.setArguments(bundle)

        fragment
    }

    /// endregion FragmentPagerAdapter members

    /// endregion Public methods
}
