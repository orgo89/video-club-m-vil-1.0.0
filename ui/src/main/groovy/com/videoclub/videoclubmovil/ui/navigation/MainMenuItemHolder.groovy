package com.videoclub.videoclubmovil.ui.navigation

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.videoclub.videoclubmovil.ui.R

/**
 * Created by Learning-03 on 04/06/2015.
 *
 * Represents the {@link android.support.v7.widget.RecyclerView.ViewHolder} item.
 */
class MainMenuItemHolder extends RecyclerView.ViewHolder {

    /// region Constructors

    /**
     * Initializes the new item holder.
     * @param itemView Current view.
     */
    MainMenuItemHolder(View itemView) {
        super(itemView)

        this.title = itemView.findViewById(R.id.menu_item_text) as TextView

        this.icon = itemView.findViewById(R.id.menu_item_icon) as ImageView
    }

    /// endregion Constructors

    /// region Properties

    /**
     * Main menu option item text.
     */
    TextView title

    /**
     * Main menu option item icon.
     */
    ImageView icon

    /// endregion Properties
}
