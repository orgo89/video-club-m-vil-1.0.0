package com.videoclub.videoclubmovil.ui

import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.FragmentActivity
import android.support.v4.view.ViewPager
import groovy.transform.CompileStatic

/**
 * Created by Learning-03 on 26/05/2015.
 *
 * Main application activity.
 */
@CompileStatic
class MainActivity extends FragmentActivity {
    /// region Variables

    /**
     * The {@link ViewPager} that will display the three primary sections of the app, one at a
     * time.
     */
    private ViewPager viewPager

    /// endregion Variables

    /// region Protected methods

    /// region Override methods

    /// region Activity lifecycle methods

    /**
     * Set comments.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.main_layout)

        this.viewPager = this.findViewById(R.id.mainActivityViewPager) as ViewPager

        this.viewPager.setPageTransformer(true, new CustomViewPagerTransform())

        def adapter = new MainActivityPageAdapter(this.getSupportFragmentManager())

        this.viewPager.adapter = adapter
    }

    /// endregion Activity lifecycle methods

    /// endregion Override methods

    /// endregion Protected methods
}
