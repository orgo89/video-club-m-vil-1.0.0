package com.videoclub.videoclubmovil.ui

import android.support.v4.view.ViewPager
import android.view.View
import groovy.transform.CompileStatic

/**
 * Created by Learning-03 on 27/05/2015.
 *
 * Represents an enhanced view pager transform.
 */
@CompileStatic
final class CustomViewPagerTransform implements ViewPager.PageTransformer {
    /// region Public methods

    /// region ViewPager.PageTransformer members

    /**
     * Apply a property transformation to the given page.
     *
     * @param page Apply the transformation to this page
     * @param position Position of page relative to the current front-and-center
     *                 position of the pager. 0 is front and center. 1 is one full
     *                 page position to the right, and -1 is one page position to the left.
     */
    @Override
    void transformPage(View view, float position) {
        def pageWidth = view.getWidth()

        def imageView = view.findViewById(R.id.main_activity_first_fragment_imageview)

        if (position < -1) { // [-Infinity,-1)

            // This page is way off-screen to the left.
            view.setAlpha(1)

        } else if (position <= 1) { // [-1,1]

            //imageView.setTranslationX(-position * (pageWidth / 2)) //Half the normal speed
            imageView.translationX = -position * (pageWidth / 2)

        } else { // (1,+Infinity]

            // This page is way off-screen to the right.
            view.setAlpha(1)

        }
    }

    /// endregion ViewPager.PageTransformer members

    /// endregion Public methods
}
