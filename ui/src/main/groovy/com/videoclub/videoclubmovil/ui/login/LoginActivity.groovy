package com.videoclub.videoclubmovil.ui.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.gc.materialdesign.views.ButtonRectangle
import com.videoclub.videoclubmovil.application.dtos.DtoResponse
import com.videoclub.videoclubmovil.application.dtos.enums.ProcessStatus
import com.videoclub.videoclubmovil.application.webservices.CustomersServiceClient
import com.videoclub.videoclubmovil.ui.R
import com.videoclub.videoclubmovil.ui.navigation.NavigationActivity
import groovy.transform.CompileStatic
import org.srs.droid.concurrent.TaskFactory

/**
 * Created by Learning-03 on 27/05/2015.
 *
 * Represents the second (login) activity fragment.
 */
@CompileStatic
final class LoginActivity extends Activity {
    /// region Variables

    /**
     * The edit text for the user name.
     */
    private EditText editTextUser

    /**
     * The hecho buton.
     */
    private ButtonRectangle buttonHecho

    /**
     * The progress bar.
     */
    private ProgressBar progressBar

    /**
     * Specifies if the hecho button would be response at the user's press action.
     */
    private boolean canPressButton

    /// endregion Variables

    /// region Constructors

    /**
     * Initializes the new object.
     */
    LoginActivity() {
        this.canPressButton = true
    }

    /// endregion Constructors

    /// region Public methods

    /// region Override methods

    /**
     * Called when the activity is starting.  This is where most initialization
     * should go: calling {@link #setContentView(int)} to inflate the
     * activity's UI, using {@link #findViewById} to programmatically interact
     * with widgets in the UI, calling
     * {@link #managedQuery(android.net.Uri, String [ ], String, String [ ], String)} to retrieve
     * cursors for data being displayed, etc.
     *
     * <p>You can call {@link #finish} from within this function, in
     * which case onDestroy() will be immediately called without any of the rest
     * of the activity lifecycle ({@link #onStart}, {@link #onResume},
     * {@link #onPause}, etc) executing.
     *
     * <p><em>Derived classes must call through to the super class's
     * implementation of this method.  If they do not, an exception will be
     * thrown.</em></p>
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *     previously being shut down then this Bundle contains the data it most
     *     recently supplied in {@link #onSaveInstanceState}.  <b><i>Note: Otherwise it is null.</i></b>
     *
     * @see #onStart
     * @see #onSaveInstanceState
     * @see #onRestoreInstanceState
     * @see #onPostCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.login_activity_layout)

        this.buttonHecho = this.findViewById(R.id.loginActivityBtnHecho) as ButtonRectangle

        this.editTextUser = this.findViewById(R.id.loginActivityEditTextUser) as EditText

        this.progressBar = this.findViewById(R.id.loginActivityProgressBar) as ProgressBar
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    void onResume() {
        super.onResume()

        this.editTextUser.onEditorActionListener = { v, actionId, event ->

            def handled = false

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                this.doneAction()

                handled = true
            }

            handled

        } as TextView.OnEditorActionListener

        this.buttonHecho.onClickListener = {
            this.doneAction()
        }
    }

    /// endregion Override methods

    /// endregion Public methods

    /// region Public methods

    /**
     * Method to be executed when the hecho button is pressed or the done keyboard action
     * is fired.
     */
    private void doneAction() {
        if (!this.canPressButton)
            return

        if (this.editTextUser.text.toString()) {

            this.canPressButton = false

            this.progressBar.visibility = ProgressBar.VISIBLE

            TaskFactory.async {

                try {

                    def response = CustomersServiceClient.existCustomer(this.editTextUser.text.toString())

                    return response

                } catch (SocketTimeoutException ex) {

                    Log.e(this.class.name, 'Error al conectar con el servicio', ex)

                    return new DtoResponse('No se pudieron obtener los datos, verifique su conexi�n a internet', ProcessStatus.ExternalError)

                } catch (Exception ex) {

                    Log.e(this.class.name, 'Error en petici�n http', ex)

                    return new DtoResponse('No se pudieron obtener los datos, verifique su conexi�n a internet', ProcessStatus.ExternalError)

                }

            } onPostExecute {

                def dto = it as DtoResponse

                if (!dto || dto.status == null || dto.message == null) {

                    Toast.makeText(this.applicationContext, 'Algo sucedió, por favor vuelva a intentarlo', Toast.LENGTH_SHORT).show()

                } else {

                    if (dto.status == ProcessStatus.Success) {

                        this.startActivity(new Intent(this, NavigationActivity.class))

                    } else if (dto.status == ProcessStatus.ExternalError) {

                        Toast.makeText(this.applicationContext, dto.message, Toast.LENGTH_LONG).show()

                    } else {

                        Log.e(this.class.name, dto.message)

                        Toast.makeText(this.applicationContext, 'Algo sucedió, por favor vuelva a intentarlo', Toast.LENGTH_SHORT).show()

                    }

                }

                this.progressBar.visibility = ProgressBar.GONE

                this.canPressButton = true
            }

            def imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            imm.hideSoftInputFromInputMethod(this.editTextUser.windowToken, 0)

        } else {

            Toast.makeText(this.applicationContext, 'Escriba su número de membresía', Toast.LENGTH_SHORT).show()

        }
    }

    /// endregion Public methods
}
