package com.videoclub.videoclubmovil.ui.navigation

import android.content.res.Configuration
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MotionEvent
import com.videoclub.videoclubmovil.ui.R

/**
 * Created by Learning-03 on 03/06/2015.
 *
 * Navigation drawer activity.
 */
class NavigationActivity extends AppCompatActivity {

    /// region Variables

    /**
     * Activity toolbar.
     */
    private Toolbar toolbar

    /**
     * Navigation drawer layout. Has the both of activity parts, the menu and the detail content.
     */
    private DrawerLayout drawerLayout

    /**
     * Add comments.
     */
    private ActionBarDrawerToggle drawerToggle

    /**
     * RecyclerView that represents the application menu.
     */
    private RecyclerView recyclerViewMainMenu

    /// endregion Variables

    /// region Public methods

    /// region AppCompatActivity members

    /**
     * Dispatch configuration change to all fragments.
     * @param newConfig The new device configuration.
     */
    @Override
    void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig)

        this.drawerToggle.onConfigurationChanged(newConfig)
    }

    /**
     * Initialize the contents of the Activity's standard options menu.  You
     * should place your menu items in to <var>menu</var>.
     *
     * <p>This is only called once, the first time the options menu is
     * displayed.  To update the menu every time it is displayed, see
     * {@link #onPrepareOptionsMenu}.
     *
     * <p>The default implementation populates the menu with standard system
     * menu items.  These are placed in the {@link Menu#CATEGORY_SYSTEM} group so that
     * they will be correctly ordered with application-defined menu items.
     * Deriving classes should always call through to the base implementation.
     *
     * <p>You can safely hold on to <var>menu</var> (and any items created
     * from it), making modifications to it as desired, until the next
     * time onCreateOptionsMenu() is called.
     *
     * <p>When you add items to the menu, you can implement the Activity's
     * {@link #onOptionsItemSelected} method to handle them there.
     *
     * @param menu The options menu in which you place your items.
     *
     * @return You must return true for the menu to be displayed;
     *         if you return false it will not be shown.
     *
     * @see #onPrepareOptionsMenu
     * @see #onOptionsItemSelected
     */
    @Override
    boolean onCreateOptionsMenu(Menu menu) {
        this.menuInflater.inflate(R.menu.navigation_menu, menu)

        true
    }

    /// endregion AppCompatActivity members

    /// endregion Public methods

    /// region Protected methods

    /// region AppCompatActivity

    /**
     * Perform initialization of all fragments and loaders.
     * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most recently supplied in onSaveInstanceState(Bundle), otherwise is null.
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.navigation_activity)

        this.getViews()

        if (this.toolbar != null) {
            this.toolbar.setTitle(this.resources.getString(R.string.toolbar_title))

            this.setSupportActionBar(this.toolbar)
        }

        this.initDrawer()
    }

    /**
     * Called when activity start-up is complete (after onStart and onRestoreInstanceState have been called). Applications will generally not implement this method; it is intended for system classes to do final initialization after application code has run.
     * Derived classes must call through to the super class's implementation of this method. If they do not, an exception will be thrown.
     * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most recently supplied in onSaveInstanceState. Note: Otherwise it is null.
     */
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState)

        this.drawerToggle.syncState()
    }

    /// endregion AppCompatActivity members

    /// endregion Protected methods

    /// region Private methods

    /**
     * This method should be called at onCreate(Bundle) method.
     *
     * Gets all the views and initializes the options list view.
     */
    private void getViews() {
        this.drawerLayout = this.findViewById(R.id.drawerLayout) as DrawerLayout

        this.toolbar = this.findViewById(R.id.toolbar) as Toolbar

        this.recyclerViewMainMenu = this.findViewById(R.id.recyclerViewMainMenu) as RecyclerView

        def adapter = new MainMenuAdapter(this, this.getMainMenuOptions())

        this.recyclerViewMainMenu.adapter = adapter

        this.addRecyclerViewMainMenuTouchListener()

        this.recyclerViewMainMenu.layoutManager = new LinearLayoutManager(this)
    }

    /**
     * Add the OnItemTouchListener event to recyclerViewMainMenu object.
     */
    private void addRecyclerViewMainMenuTouchListener() {
        this.recyclerViewMainMenu.addOnItemTouchListener([
                onInterceptTouchEvent: { RecyclerView rv, MotionEvent e ->

                    def view = rv.findChildViewUnder(e.x, e.y)

                    if (view != null) {
                        def position = rv.getChildLayoutPosition(view)

                        if (position == 0) {
                            return false
                        }
                    }

                    this.drawerLayout.closeDrawer(GravityCompat.START)

                    return false
                }, onTouchEvent      : { RecyclerView recyclerView, MotionEvent motionEvent ->

        }] as RecyclerView.OnItemTouchListener)
    }

    /**
     * Initializes the navigation drawer.
     */
    private void initDrawer() {
        this.drawerToggle = new ActionBarDrawerToggle(this, this.drawerLayout, this.toolbar, R.string.drawer_open, R.string.drawer_close)

        this.drawerLayout.drawerListener = this.drawerToggle
    }

    /**
     * Gets the main menu options list.
     * @return A {@link List} with certain options.
     */
    private List<MainMenuItem> getMainMenuOptions() {
        def optionsList = [new MainMenuItem(R.drawable.smallvideo4, 'Pelis'),
                           new MainMenuItem(R.drawable.smallvideo3, 'Rentas'),
                           new MainMenuItem(R.drawable.charge, 'Recargos')]

        optionsList
    }

    /// endregion Private methods
}
