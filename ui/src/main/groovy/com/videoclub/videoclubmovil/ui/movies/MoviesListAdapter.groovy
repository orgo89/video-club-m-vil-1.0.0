package com.videoclub.videoclubmovil.ui.movies

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.videoclub.videoclubmovil.application.dtos.DtoMovie
import com.videoclub.videoclubmovil.ui.R

/**
 * Created by Learning-03 on 05/06/2015.
 *
 * Movie CardView layout adpater.
 * Provides the inflation action to each one of movie views.
 */
class MoviesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /// region Variables

    /**
     * Current existing movies.
     */
    private List<DtoMovie> movies

    /**
     * Current layout inflater.
     */
    private LayoutInflater layoutInflater

    /// endregion Variables

    /// region Constructors

    /**
     * Initializes the new object.
     *
     * @param currentContext Current application context.
     * @param movies Cirrent movies list.
     */
    MoviesListAdapter(Context currentContext, List<DtoMovie> movies) {
        this.movies = movies

        this.layoutInflater = LayoutInflater.from(currentContext)
    }

    /// endregion Constructors

    /// region Public methods

    /// region RecyclerView.Adapter members

    /**
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     * </br>
     * This new ViewHolder should be constructed with a new View that can represent the items of the given type. You can either create a new View manually or inflate it from an XML layout file.
     * </br>
     * The new ViewHolder will be used to display items of the adapter using onBindViewHolder(ViewHolder, int). Since it will be re-used to display different items in the data set, it is a good idea to cache references to sub views of the View to avoid unnecessary findViewById(int) calls.
     * @param viewGroup The ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @Override
    RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        def view = this.layoutInflater.inflate(R.layout.movie_card_view_layout, viewGroup, false)

        new MovieCardViewHolder(view)
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should update the contents of the itemView to reflect the item at the given position.
     * </br>
     * Note that unlike ListView, RecyclerView will not call this method again if the position of the item changes in the data set unless the item itself is invalidated or the new position cannot be determined. For this reason, you should only use the position parameter while acquiring the related data item inside this method and should not keep a copy of it. If you need the position of an item later on (e.g. in a click listener), use getAdapterPosition() which will have the updated adapter position.
     * @param viewHolder The ViewHolder which should be updated to represent the contents of the item at the given position in the data set.
     * @param i The position of the item within the adapter's data set.
     */
    @Override
    void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        def holder = viewHolder as MovieCardViewHolder

        holder.movieAvailability.text = this.movies[i - 1].availability ? 'Si' : 'No'
        holder.movieGenres.text = this.movies[i - 1].genres.join(', ')
        holder.movieTitlte.text = this.movies[i - 1].title
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     * @return the stable ID of the item at position.
     */
    @Override
    int getItemCount() {
        this.movies.size() + 1
    }

    /// endregion RecyclerView.Adapter members

    /// endregion Public methods
}
