package com.videoclub.videoclubmovil.ui.navigation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.videoclub.videoclubmovil.ui.R

/**
 * Created by Learning-03 on 04/06/2015.
 *
 * The {@link RecyclerView}  main menu adapter.
 */
class MainMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /// region Variables

    /**
     * Byte constant value, specifies that current RecyclerView item it's a header.
     */
    private static final int ITEM_TYPE_HEADER = 0

    /**
     * Byte constant value, specifies that current RecyclerView item it's a simple item.
     */
    private static final int ITEM_TYPE_ITEM = 1

    /**
     * Current application context.
     */
    private Context currentContext

    /**
     * Current movie list.
     */
    private List<MainMenuItem> data

    /**
     * Current layout inflater to fill view layouts.
     */
    private LayoutInflater inflater

    /// endregion Variables

    /// region Constructors

    /**
     * Initializes the new object.
     */
    MainMenuAdapter(Context currentContext, List<MainMenuItem> data) {
        this.currentContext = currentContext

        this.data = data

        this.inflater = LayoutInflater.from(currentContext)
    }

    /// endregion Constructors

    /// region Public methods

    /// region Members of RecyclerView.Adapter

    /**
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     * </br>
     * This new ViewHolder should be constructed with a new View that can represent the items of the given type. You can either create a new View manually or inflate it from an XML layout file.
     * </br>
     * The new ViewHolder will be used to display items of the adapter using onBindViewHolder(ViewHolder, int). Since it will be re-used to display different items in the data set, it is a good idea to cache references to sub views of the View to avoid unnecessary findViewById(int) calls.
     * @param viewGroup The ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @Override
    RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType.equals(ITEM_TYPE_HEADER)) {
            def view = this.inflater.inflate(R.layout.main_menu_header, viewGroup, false)

            new MainMenuHeaderHolder(view)
        } else {
            def view = this.inflater.inflate(R.layout.main_menu_item, viewGroup, false)

            new MainMenuItemHolder(view)
        }
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should update the contents of the itemView to reflect the item at the given position.
     * </br>
     * Note that unlike ListView, RecyclerView will not call this method again if the position of the item changes in the data set unless the item itself is invalidated or the new position cannot be determined. For this reason, you should only use the position parameter while acquiring the related data item inside this method and should not keep a copy of it. If you need the position of an item later on (e.g. in a click listener), use getAdapterPosition() which will have the updated adapter position.
     * @param viewHolder The ViewHolder which should be updated to represent the contents of the item at the given position in the data set.
     * @param i The position of the item within the adapter's data set.
     */
    @Override
    void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof MainMenuItemHolder) {
            def holder = viewHolder as MainMenuItemHolder

            def item = this.data[i - 1]

            holder.title.text = item.text
            holder.icon.setImageResource(item.iconResId)
        }
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     * @return the stable ID of the item at position.
     */
    @Override
    int getItemCount() {
        this.data.size() + 1
    }

    /**
     * Gets the item type to be created according to the specified position.
     * @param position Current view position in the Recycler view.
     * @return An integer number that represents the item type.
     */
    @Override
    int getItemViewType(int position) {
        position == 0 ? ITEM_TYPE_HEADER : ITEM_TYPE_ITEM
    }
/// endregion Members of RecyclerView.Adapter

    /// endregion Public methods
}
