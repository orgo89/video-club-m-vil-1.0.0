package com.videoclub.videoclubmovil.application.mappings

import com.google.gson.JsonObject
import com.videoclub.videoclubmovil.application.dtos.DtoResponse
import com.videoclub.videoclubmovil.application.dtos.enums.ProcessStatus
import org.modelmapper.AbstractConverter

/**
 * Created by Learning-03 on 03/06/2015.
 *
 * Defines the way to map a string response to {@link DtoResponse} enum.
 */
class ProcessStatusConverter extends AbstractConverter<JsonObject, DtoResponse> {

    /**
     * Converts {@code source} to an instance of type {@code D}.
     */
    @Override
    protected DtoResponse convert(JsonObject source) {
        ProcessStatus processStatus

        def strStatus = source.get('Status').asString

        // Gets the status as string and not as number to enable comparisons both for the enum value and
        // the enum name.
        switch (strStatus) {
            case ['0', 'Success']:
                processStatus = ProcessStatus.Success
                break

            case ['1', 'InternalError']:
                processStatus = ProcessStatus.InternalError
                break

            default:
                processStatus = ProcessStatus.ExternalError
                break
        }

        def element = source.get('Message')

        def message = element.jsonNull ? '' : element.asString

        new DtoResponse(message, processStatus)
    }
}
