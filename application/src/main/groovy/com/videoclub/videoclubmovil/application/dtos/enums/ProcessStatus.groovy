package com.videoclub.videoclubmovil.application.dtos.enums

import groovy.transform.CompileStatic

/**
 * Created by Learning-03 on 28/05/2015.
 *
 * Enumerates different process status.
 */
@CompileStatic
enum ProcessStatus {

    /**
     * Specifies a success process.
     */
    Success(0, 'Success'),

    /**
     * Specifies that the process finished with internal error.
     */
            InternalError(1, 'InternalError'),

    /**
     * Specifies that the process finished with external error like receives bad parameters.
     */
            ExternalError(2, 'ExternalError')

    /**
     * Current enum value.
     */
    private final int value

    /**
     * Current enum value name.
     */
    private final String name

    /**
     * Initializes the enum.
     * @param value Enum value.
     */
    ProcessStatus(int value, String name) {
        this.value = value

        this.name = name
    }

    /**
     * Gets the current enum value.
     * @return Integer value.
     */
    int value() {
        this.value
    }
}