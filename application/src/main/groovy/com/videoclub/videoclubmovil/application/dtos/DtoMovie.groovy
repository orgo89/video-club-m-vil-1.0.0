package com.videoclub.videoclubmovil.application.dtos

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.transform.TupleConstructor

/**
 * Created by Learning-03 on 04/06/2015.
 *
 * Dto that represents a movie.
 */
@EqualsAndHashCode
@TupleConstructor
@ToString(includeFields = true, includeNames = true, includePackage = true)
class DtoMovie {

    /// region Properties

    /**
     * Movie title.
     */
    String title

    /**
     * Movie genres list.
     */
    List<String> genres

    /**
     * Movie availability.
     */
    Boolean availability

    /// endregion Properties

}
