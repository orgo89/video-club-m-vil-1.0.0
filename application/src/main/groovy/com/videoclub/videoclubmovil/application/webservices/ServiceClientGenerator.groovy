package com.videoclub.videoclubmovil.application.webservices

import groovy.transform.TupleConstructor
import retrofit.RestAdapter
/**
 * Created by Learning-03 on 28/05/2015.
 *
 * Contains methods to get web service clients.
 */
@TupleConstructor
final class ServiceClientGenerator {

    /// region Variables

    /**
     * String object that represents the web service endpoint.
     */
    private final String webServicesEndPoint = 'http://192.168.15.60:9001/VideoClubService'

    /// endregion Variables

    /// region Public methods

    /**
     * Creates a new customers service client.
     * @return New customers service client.
     */
    ICustomersService createCustomersClient() {
        def adapter = this.createRestAdapter()

        def client = adapter.create(ICustomersService)

        client
    }

    ///endregion Public methods

    /// region Private methdos

    /**
     * Creates and Gets the rest adapter to connect with the web services.
     * @return New rest adapter.
     */
    private RestAdapter createRestAdapter() {
        new RestAdapter.Builder()
                .setEndpoint(this.webServicesEndPoint)
                .build()
    }

    /// endregion Private methods
}
