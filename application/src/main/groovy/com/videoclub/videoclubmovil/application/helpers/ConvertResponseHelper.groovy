package com.videoclub.videoclubmovil.application.helpers

import com.google.gson.JsonParser
import com.videoclub.videoclubmovil.application.dtos.DtoResponse
import com.videoclub.videoclubmovil.application.dtos.enums.ProcessStatus
import com.videoclub.videoclubmovil.application.mappings.ProcessStatusConverter
import org.modelmapper.ModelMapper
import org.modelmapper.gson.JsonElementValueReader
import retrofit.client.Response

/**
 * Created by Learning-03 on 03/06/2015.
 *
 * Helper class to obtain dto responses from {@link Response} objects.
 */
class ConvertResponseHelper {

    /// region Public static methods

    /**
     * Converts the specified {@link Response} object to a {@link DtoResponse} object.
     *
     * Analizes the web status and the response status.
     * @param response Web service call response.
     * @return Web service call response as {@link DtoResponse}.
     */
    static DtoResponse convertResponseToDtoResponse(Response response) {

        // First check the web status, should be 200
        if (response.status == 200) {
            def jsonString = response.body.in().text

            def mapper = new ModelMapper()

            mapper.configuration.addValueReader(new JsonElementValueReader())

            mapper.addConverter(new ProcessStatusConverter())

            def jsonObject = new JsonParser().parse(jsonString)

            def dto = mapper.map(jsonObject, DtoResponse.class)

            dto
        } else {
            new DtoResponse(response.reason, ProcessStatus.ExternalError)
        }
    }

    /// endregion Public static methods

}
