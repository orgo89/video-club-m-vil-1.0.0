package com.videoclub.videoclubmovil.application.webservices

import retrofit.client.Response
import retrofit.http.GET
import retrofit.http.Path
import retrofit.http.Streaming
/**
 * Created by Learning-03 on 28/05/2015.
 *
 * Customers service operation contract.
 */
interface ICustomersService {

    /// region Methods

    /**
     * Verifies if the given customer exists.
     * @param customerNumber Customer number.
     * @return Simple dto response that specifies if exists.
     */
    @GET('/Customers/ExistsCustomer/{CustomerNumber}')
    @Streaming
    Response existCustomer(@Path('CustomerNumber') String customerNumber)

    /// endregion Methods

}