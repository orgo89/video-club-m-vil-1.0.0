package com.videoclub.videoclubmovil.application.dtos
import com.videoclub.videoclubmovil.application.dtos.enums.ProcessStatus
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.transform.TupleConstructor
/**
 * Created by Learning-03 on 28/05/2015.
 *
 * Simple dto response.
 */
@EqualsAndHashCode
@TupleConstructor
@ToString(includeFields = true, includeNames = true, includePackage = true)
class DtoResponse {

    /// region Properties

    /**
     * Process generated message.
     */
    String message

    /**
     * Process status.
     */
    ProcessStatus status

    /// endregion Properties

}
