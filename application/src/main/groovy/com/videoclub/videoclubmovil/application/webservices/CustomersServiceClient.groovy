package com.videoclub.videoclubmovil.application.webservices

import com.videoclub.videoclubmovil.application.dtos.DtoResponse
import com.videoclub.videoclubmovil.application.helpers.ConvertResponseHelper

/**
 * Created by Learning-03 on 29/05/2015.
 *
 * Customers web service client.
 */
class CustomersServiceClient {

    /// region Public static methods

    /**
     * Try to get if the customer number exist.
     * @param customerNumber Customer number.
     * @return Simple dto response.
     */
    static DtoResponse existCustomer(String customerNumber) {

        def client = new ServiceClientGenerator().createCustomersClient()

        def response = client.existCustomer(customerNumber)

        ConvertResponseHelper.convertResponseToDtoResponse(response)

    }

    /// endregion Public static methods

}
